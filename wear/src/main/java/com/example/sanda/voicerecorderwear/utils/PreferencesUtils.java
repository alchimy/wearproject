package com.example.sanda.voicerecorderwear.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by sanda on 11.03.2018.
 */

public class PreferencesUtils {
    private final SharedPreferences sharedPreferences;
    private final String KEY_ACCESS_TOKEN = "access_token";

    public PreferencesUtils(Context context) {
        this.sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

    }

    public void setAccessToken(String accessToken) {
        sharedPreferences.edit()
                .putString(KEY_ACCESS_TOKEN, accessToken)
                .apply();
    }

    public String getAccessToken(){
        return sharedPreferences.getString(KEY_ACCESS_TOKEN, " ");
    }


}
