package com.example.sanda.voicerecorderwear.api.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Ryzen on 3/8/2018.
 */

public class Human {
    @SerializedName("name")
    private String humanName;
    @SerializedName("surname")
    private String humanSurname;
    @SerializedName("age")
    private int humanAge;

    public Human(String humanName, String humanSurname, int humanAge) {
        this.humanName = humanName;
        this.humanSurname = humanSurname;
        this.humanAge = humanAge;
    }

    public String getHumanName() {
        return humanName;
    }

    public void setHumanName(String humanName) {
        this.humanName = humanName;
    }

    public String getHumanSurname() {
        return humanSurname;
    }

    public void setHumanSurname(String humanSurname) {
        this.humanSurname = humanSurname;
    }

    public int getHumanAge() {
        return humanAge;
    }

    public void setHumanAge(int humanAge) {
        this.humanAge = humanAge;
    }

    @Override
    public String toString() {
        return "Human{" +
                "humanName='" + humanName + '\'' +
                ", humanSurname='" + humanSurname + '\'' +
                ", humanAge=" + humanAge +
                '}';
    }
}
