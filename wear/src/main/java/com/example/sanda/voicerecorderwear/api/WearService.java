package com.example.sanda.voicerecorderwear.api;

import android.app.VoiceInteractor;
import android.media.AudioManager;

import com.example.sanda.voicerecorderwear.api.model.Stt;
import com.example.sanda.voicerecorderwear.api.model.mainModel.InvokeModel;

import java.util.List;

import io.reactivex.Observable;
import okhttp3.MediaType;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by Ryzen on 3/8/2018.
 */

public interface WearService {
    String INVOKE = "/cvi/dm/api/v1/invoke/text/json";
    String TTS = "cvi/voice/api/v1/tts";
   // String LOGIN = "/cvi/user/api/v1/login";




// TODO: 11.03.2018 change name when i obtine a documentation

    @POST(INVOKE)
    Observable<Response<InvokeModel>> invoke(@Body String responseText);

    @POST(TTS)
    Observable<Response<ResponseBody>> tts(@Body String speech);

//
//    @POST(LOGIN)
//    Observable<Response<RequestBody>> login();
//
//    @POST(HELLO)
//    Observable<Response<String>> hello();
//
//    @POST(GET_HUMAN)
//    Observable<Response<Human>> getHuman();
//
//    @POST(BUILD_HUMAN)
//    Observable<Response<Human>> buildHuman(@Field("name") String humanName, @Field("surname") String humanSurname, @Field("age") int humanAge);
//
//    @POST(PROCESS_SPEECH)
//    @FormUrlEncoded
//    Observable<Response<List<String>>> speechList(@Field("speech") String say);


}
