package com.example.sanda.voicerecorderwear.api.repository;

import android.app.VoiceInteractor;
import android.content.Context;
import android.media.AudioManager;

import com.example.sanda.voicerecorderwear.api.ServiceFactory;
import com.example.sanda.voicerecorderwear.api.WearService;
import com.example.sanda.voicerecorderwear.api.model.Stt;
import com.example.sanda.voicerecorderwear.api.model.mainModel.InvokeModel;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * Created by Ryzen on 3/8/2018.
 */

// TODO: 11.03.2018 change name when i obtine a documentation

public class InvokeRepository {
    private WearService wearService;

    public InvokeRepository(Context context) {
        this.wearService = ServiceFactory.makeService(context);
    }

    public Observable<InvokeModel> invokeModelObservable(String responseText){
        return wearService.invoke(responseText)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(Response::body);

    }

    public Observable<ResponseBody> ttsModelObservable(String speech){
        return wearService.tts(speech)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(Response::body);
    }

//    public Observable<String> hello() {
//        return wearService.hello()
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .map(Response::body);
//    }
//
//    public Observable<Human> getHuman() {
//        return wearService.getHuman()
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .map(Response::body);
//    }
//
//    public Observable<Human> buildHuman(String name, String surname, int age) {
//        return wearService.buildHuman(name, surname, age)
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .map(Response::body);
//    }
//
//    public Observable<List<String>> processSpeech(String speech){
//        return wearService.speechList(speech)
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .map(Response::body);
//    }
}
