package com.example.sanda.voicerecorderwear.api.model.mainModel;

import com.example.sanda.voicerecorderwear.api.model.Session;
import com.example.sanda.voicerecorderwear.api.model.Stt;
import com.example.sanda.voicerecorderwear.api.model.SttCandidates;
import com.example.sanda.voicerecorderwear.api.model.intent.IntentModel;
import com.example.sanda.voicerecorderwear.api.model.skill.Skill;
import com.example.sanda.voicerecorderwear.main.MainWearActivity;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sanda on 09.03.2018.
 */

// TODO: 11.03.2018 change name when i obtine a documentation

public class InvokeModel {
    private String text;
    private Stt stt;
    private SttCandidates sttCandidates;
    private IntentModel intent;
    private Skill skill;
    private Session session;

    public InvokeModel(String text, Stt stt, SttCandidates sttCandidates, IntentModel intent, Skill skill, Session session) {
        this.text = text;
        this.stt = stt;
        this.sttCandidates = sttCandidates;
        this.intent = intent;
        this.skill = skill;
        this.session = session;
    }


    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Stt getStt() {
        return stt;
    }

    public void setStt(Stt stt) {
        this.stt = stt;
    }

    public SttCandidates getSttCandidates() {
        return sttCandidates;
    }

    public void setSttCandidates(SttCandidates sttCandidates) {
        this.sttCandidates = sttCandidates;
    }

    public IntentModel getIntent() {
        return intent;
    }

    public void setIntent(IntentModel intent) {
        this.intent = intent;
    }

    public Skill getSkill() {
        return skill;
    }

    public void setSkill(Skill skill) {
        this.skill = skill;
    }

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }
}
