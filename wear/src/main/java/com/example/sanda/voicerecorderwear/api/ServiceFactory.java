package com.example.sanda.voicerecorderwear.api;

import android.content.Context;

import com.example.sanda.voicerecorderwear.BuildConfig;
import com.example.sanda.voicerecorderwear.R;
import com.example.sanda.voicerecorderwear.api.interceptors.HeaderInterceptor;
import com.example.sanda.voicerecorderwear.utils.PreferencesUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;

import okhttp3.Cache;
import okhttp3.OkHttpClient;

import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Ryzen on 3/8/2018.
 */

public class ServiceFactory {
    private PreferencesUtils preferencesUtils;
    private Context mContext;

    public ServiceFactory(PreferencesUtils preferencesUtils, Context mContext) {
        this.preferencesUtils = preferencesUtils;
        this.mContext = mContext;
    }

    public static WearService makeService(Context context) {
        String baseUrl = context.getString(R.string.base_url);
        //setup cache
        File httpCacheDirectory = new File(context.getCacheDir(), "response");
        int cacheSize = 10 * 1024 * 1024; // 10 MiB
        Cache cache = new Cache(httpCacheDirectory, cacheSize);

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        HttpLoggingInterceptor.Level logLevel =
                BuildConfig.DEBUG ? HttpLoggingInterceptor.Level.BODY : HttpLoggingInterceptor.Level.NONE;
        logging.setLevel(logLevel);

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(logging)
                .addInterceptor(new HeaderInterceptor(context))
                .cache(cache)
                .build();


        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okHttpClient)
                .build();

        return retrofit.create(WearService.class);
    }


}
