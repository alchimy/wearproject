package com.example.sanda.voicerecorderwear.api.interceptors;

import android.content.Context;
import android.util.Log;

import com.example.sanda.voicerecorderwear.R;
import com.example.sanda.voicerecorderwear.api.WearService;
import com.example.sanda.voicerecorderwear.utils.PreferencesUtils;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

import static android.content.ContentValues.TAG;

/**
 * Created by sanda on 11.03.2018.
 */

public class HeaderInterceptor implements Interceptor {
    private final PreferencesUtils preferencesUtils;
    private Context mContext;

    public HeaderInterceptor(Context context) {
        this.preferencesUtils = new PreferencesUtils(context);
        this.mContext = context;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request newRequest = chain.request();
        String apiKey = "b507d7ad-9e14-4a26-a3b5-0cc4ec2a2da9";

        Response originalResponse = chain.proceed(newRequest);
        String url = originalResponse.request().url().url().toString();

        Request.Builder requestBuilder = newRequest.newBuilder()
//                .header("Accept","audio/wav")
                .header("Accept", url.contains(WearService.TTS) ? "audio/wav" : "application/json")
                .header("Content-Type", "application/json")
                .header("X-Consumer-Username", "smarthub_nuance")
                .header("apiKey", apiKey)
                .header("Authorization", mContext.getString(R.string.bearer, preferencesUtils.getAccessToken()));

        return chain.proceed(requestBuilder.build());
    }
}
