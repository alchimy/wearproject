package com.example.sanda.voicerecorderwear.main;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.wear.widget.SwipeDismissFrameLayout;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;

import com.example.sanda.voicerecorderwear.R;
import com.example.sanda.voicerecorderwear.api.repository.InvokeRepository;
import com.example.sanda.voicerecorderwear.utils.PreferencesUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Locale;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import okhttp3.ResponseBody;

public class
MainWearActivity extends Activity {
    private final String TAG = getClass().getSimpleName();

    Button speakButton;
    Button replyButton;
    private TextView txtResult;

    private static final int SPEECH_REQUEST_CODE = 10;
    private static final int SPEECH_REQUEST_CODE_WITH_MAIN_ACTIVITY_CLOSE = 20;
    private static final String EXTRA_VOICE_REPLY = "extra_voice_reply";

    private InvokeRepository invokeRepository;
    private String firstRequestResult;

    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_wear);

//        replyButton = findViewById(R.id.replyButton);
//        speakButton = findViewById(R.id.speakButton);
        txtResult = findViewById(R.id.txtResult);

        // call class who is responsible for request
        invokeRepository = new InvokeRepository(this);

        displaySpeechRecognize(SPEECH_REQUEST_CODE);

        SwipeDismissFrameLayout dismissLayout = findViewById(R.id.swipeDismissLayout);
        dismissLayout.addCallback(new SwipeDismissFrameLayout.Callback() {
            @Override
            public void onSwipeStarted(SwipeDismissFrameLayout layout) {
                super.onSwipeStarted(layout);
                Log.e(TAG, "onSwipeStarted: ");
            }

            @Override
            public void onSwipeCanceled(SwipeDismissFrameLayout layout) {
                super.onSwipeCanceled(layout);
                Log.e(TAG, "onSwipeCanceled: ");
            }

            @Override
            public void onDismissed(SwipeDismissFrameLayout layout) {
                Log.e(TAG, "onDismissed: ");
                displaySpeechRecognize(SPEECH_REQUEST_CODE_WITH_MAIN_ACTIVITY_CLOSE);
            }
        });

    }

    //in this method i use the Intent constant for supporting speech recognition
    //speech recognition start an Intent
    private void displaySpeechRecognize(int requestCode) {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.GERMAN.toLanguageTag());
        startActivityForResult(intent, requestCode);
    }

    //here is happening callback (say word, and receive response)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && (requestCode == SPEECH_REQUEST_CODE ||
                requestCode == SPEECH_REQUEST_CODE_WITH_MAIN_ACTIVITY_CLOSE)) {
            List<String> results = data.getStringArrayListExtra(
                    RecognizerIntent.EXTRA_RESULTS);
            String spokenText = results.get(0);
            String voice = results.get(0);

            // PreferenceUtils class when we declare/keep token
            new PreferencesUtils(this).setAccessToken("eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJnaGVvcmdoZS5zdGFuY292QGdtYWlsLmNvbSIsImF1ZCI6WyJzdmhfYmFja2VuZCIsImN2aV9jb3JlIiwid3V3X3N0b3JhZ2UiXSwidHJhY2luZyI6ZmFsc2UsIm5iZiI6MTUyMTE5MzIzOCwicHJvZmlsZSI6Mzk2MDAsImlzcyI6ImN2aV9jb3JlIiwiZXhwIjoxNTIxNzk4MDQzLCJpYXQiOjE1MjExOTMyNDMsInRlbmFudCI6InNtYXJ0aHViX251YW5jZSJ9.mmpHRZeyxwZRC_9cCspHVa_NLOu3OahojmMaum6SaUA");

            performFirstRequest(spokenText);

            super.onActivityResult(requestCode, resultCode, data);
        }
        if (resultCode == RESULT_CANCELED && requestCode == SPEECH_REQUEST_CODE_WITH_MAIN_ACTIVITY_CLOSE) {
            finish();
        }
    }

    private void performFirstRequest(String spokenText) {
        animateLoading();
        invokeRepository.invokeModelObservable(spokenText)
                .subscribe(invokeMode1 -> {
                            firstRequestResult = invokeMode1.getText();
                            transformTextToWav(invokeMode1.getText());
                        },
                        exception -> new AlertDialog.Builder(this)
                                .setMessage(exception.getMessage())
                                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                })
                                .setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        performFirstRequest(spokenText);
                                        dialog.dismiss();
                                    }
                                })
                                .show()
                        , () -> Log.e(TAG, "onComplete"));
    }

    private void animateLoading() {
        txtResult.clearAnimation();
        txtResult.setText(R.string.loading);
        Animation fadeAnimation = AnimationUtils.loadAnimation(this, R.anim.fade);
        txtResult.startAnimation(fadeAnimation);
    }

    private void animateTextChanges(String newText) {
        txtResult.clearAnimation();
        txtResult.animate()
                .alpha(0)
                .setDuration(1_000)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        txtResult.setText(newText);
                        txtResult.animate()
                                .alpha(1)
                                .setDuration(1_000)
                                .start();
                    }
                })
                .start();
    }

    public void transformTextToWav(String text) {
        invokeRepository.ttsModelObservable(text)
                .subscribe(new Observer<ResponseBody>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(ResponseBody responseBody) {
                        animateTextChanges(firstRequestResult);
                        InputStream inputStream = responseBody.byteStream();
                        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
                        int len = 0;
                        try {
                            int bufferSize = 1024;
                            byte[] buffer = new byte[bufferSize];
                            while ((len = inputStream.read(buffer)) != -1) {
                                byteBuffer.write(buffer, 0, len);
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        playResponse(byteBuffer.toByteArray());

                        Log.e(TAG, "onNext: ");
                    }

                    @Override
                    public void onError(Throwable e) {
                        animateTextChanges(firstRequestResult);
                        Log.e(TAG, "onError: ", e);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
    private void playResponse(byte[] audioFileWavFormant) {
        new Thread(() -> {
            try {
                File tempWav = new File(getCacheDir(), "response.wav");
                tempWav.deleteOnExit();
                FileOutputStream fos = new FileOutputStream(tempWav);
                fos.write(audioFileWavFormant);
                fos.close();

                FileInputStream fis = new FileInputStream(tempWav);
                int minBufferSize = AudioTrack.getMinBufferSize(16_000,
                        AudioFormat.CHANNEL_OUT_MONO, AudioFormat.ENCODING_PCM_16BIT);
                AudioTrack at = new AudioTrack(AudioManager.STREAM_MUSIC, 16_000,
                        AudioFormat.CHANNEL_OUT_MONO, AudioFormat.ENCODING_PCM_16BIT,
                        minBufferSize, AudioTrack.MODE_STREAM);

                int i = 0;
                byte[] music = null;
                try {
                    music = new byte[512];
                    at.play();

                    while ((i = fis.read(music)) != -1)
                        at.write(music, 0, i);

                } catch (IOException e) {
                    e.printStackTrace();
                }

                at.stop();
                at.release();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }).start();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


}
