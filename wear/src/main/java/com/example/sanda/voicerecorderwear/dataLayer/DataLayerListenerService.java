package com.example.sanda.voicerecorderwear.dataLayer;

import android.net.Uri;

import com.google.android.gms.common.data.FreezableUtils;
import com.google.android.gms.wearable.DataEvent;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.DataMapItem;
import com.google.android.gms.wearable.WearableListenerService;

import java.util.List;

/**
 * Created by sanda on 06.03.2018.
 */

public class DataLayerListenerService extends WearableListenerService {

    @Override
    public void onDataChanged(DataEventBuffer dataEventBuffer) {
        super.onDataChanged(dataEventBuffer);

        final List<DataEvent> events = FreezableUtils.freezeIterable(dataEventBuffer);
        for (DataEvent event : events){
            final Uri uri = event.getDataItem().getUri();
            final String path = uri != null ? uri.getPath() : null;

//            if ("/Sample".equals(path)){
//                final DataMap map = DataMapItem.fromDataItem(event.getDataItem().getDataMap());
//                int color = map.getInt("color");
//                String stringExample = map.getString("string_example");
//            }
        }
    }
}
