package com.example.sanda.voicerecorderwear.notificaton;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.TextView;

import com.example.sanda.voicerecorderwear.R;

/**
 * Created by sanda on 05.03.2018.
 */

public class NotificationActivity extends Activity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_notification);

        Intent intent = getIntent();

        TextView replyTextView = findViewById(R.id.txtViewReply);
        replyTextView.setText(getMessageText(intent));
    }

    private int getMessageText(Intent intent) {

        return Integer.parseInt(null);
    }
}
