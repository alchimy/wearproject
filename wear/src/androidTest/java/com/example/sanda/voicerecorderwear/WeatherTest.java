package com.example.sanda.voicerecorderwear;

import android.support.test.annotation.UiThreadTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.example.sanda.voicerecorderwear.main.MainWearActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * Created by Ryzen on 3/15/2018.
 */

@RunWith(AndroidJUnit4.class)
public class WeatherTest {
    @Rule
    public ActivityTestRule<MainWearActivity> activityTestRule =
            new ActivityTestRule<>(MainWearActivity.class);

    @Test
    @UiThreadTest
    public void weatherRequestTest() throws Exception {
        activityTestRule.getActivity().transformTextToWav("Ich habe versucht, die Antwort vom Server zu bekommen, aber es tränkt nicht, so dass ich die Antwort nicht bekommen konnte");

        Thread.sleep(10_000);
    }
}
